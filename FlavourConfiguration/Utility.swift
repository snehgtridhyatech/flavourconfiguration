//
//  Utility.swift
//  FlavourConfiguration
//
//  Created by Sneh on 01/01/24.
//

import Foundation


struct EnvironmentKeys {
    static let base_url = "base_url"
    static let mode = "mode"
}


struct EnvironmentConfig {
    
    public static func getModeType() -> String {
        return "\(EnvironmentUtility().getValueFromMainInfoDictionary(key: EnvironmentKeys.mode))"
    }
    
    public static func getBaseUrl() -> String {
        return "http://\(EnvironmentUtility().getValueFromMainInfoDictionary(key: EnvironmentKeys.base_url))"
    }
}

fileprivate struct EnvironmentUtility {
    
    fileprivate var info_dictionary: [String: Any] {
        return Bundle.main.infoDictionary ?? [String: Any]()
    }
    
    fileprivate func getValueFromMainInfoDictionary(key: String) -> String {
        return info_dictionary[key] as? String ?? ""
    }
    
}
