//
//  ViewController.swift
//  FlavourConfiguration
//
//  Created by Sneh on 30/12/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var lblMode: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblMode.text = EnvironmentConfig.getModeType()
        self.lblUrl.text = EnvironmentConfig.getBaseUrl()
        
    }
}

